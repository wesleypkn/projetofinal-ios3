//
//  TaskListTableViewCell.swift
//  ProjetoFinal-iOS3
//
//  Created by Wesley Brito on 02/07/2018.
//  Copyright © 2018 Wesley Brito. All rights reserved.
//

import UIKit

class TaskListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var taskTitle: UILabel!
    
    @IBOutlet weak var taskDate: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func prepare(with task: Task) {
        taskTitle.text = task.title
        
        //Date Formatter
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let newDate = dateFormatter.string(from: task.date!)
        
        
        taskDate.text = newDate
    }

}
