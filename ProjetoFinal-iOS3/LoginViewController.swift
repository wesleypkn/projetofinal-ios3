//
//  LoginViewController.swift
//  ProjetoFinal-iOS3
//
//  Created by Wesley Brito on 29/06/2018.
//  Copyright © 2018 Wesley Brito. All rights reserved.
//

import UIKit
import FacebookLogin
import FirebaseAuth
import FBSDKLoginKit
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
    }
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var facebookButton: UIButton!
    
    @IBOutlet weak var googleButton: UIButton!
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var senhaText: UITextField!
    
    @IBAction func loginButton(_ sender: Any) {
        loginEmail()
    }
    
    @IBAction func facebookLogin(_ sender: Any) {
        loginFacebook()
    }
    
    @IBAction func googleLogin(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
        if Auth.auth().currentUser != nil {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func exibirMensagem(titulo: String, mensagem: String){
        
        let alerta = UIAlertController(title: titulo, message: mensagem, preferredStyle: .alert)
        let acaoCancelar = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alerta.addAction( acaoCancelar )
        present(alerta, animated: true, completion: nil)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func loginEmail() {
        if let email = self.emailText.text {
            if let senha = self.senhaText.text{
                
                //Autenticar usuario firebase
                let autenticacao = Auth.auth()
                autenticacao.signIn(withEmail: email, password: senha, completion: { (usuario, erro) in
                    
                    if erro == nil {
                        if usuario == nil{
                            
                            self.exibirMensagem(titulo: "Erro ao autenticar", mensagem: "Erro ao autenticar, tente novamente.")
                            
                        }else{
                            
                            self.exibirMensagem(titulo: "Sucesso", mensagem: "Usuário logado com sucesso.")
                            //redireciona quando usuário é autenticao
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                    }else{
                        //mensagem autenticação
                        self.exibirMensagem(titulo: "Dados incorrentos", mensagem: "Verifique os dados digitados e tente novamente.")
                    }
                })
                
            }
        }
    }
    
    func loginFacebook() {
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                self.exibirMensagem(titulo: "Login Facebook", mensagem: "Erro para efetuar login com o Facebook !")
                return
            }
            
            guard let accessToken = FBSDKAccessToken.current() else {
                print("Failed to get access token")
                return
            }
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            // Autenticação login facebook `credential` como token
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                if let error = error {
                    print("Login error: \(error.localizedDescription)")
                    self.exibirMensagem(titulo: "Login Facebook", mensagem: "Erro para autenticação por favor refaça a operação")
                    
                    return
                }
                //chamada metodo de redirecionamento
                self.dismiss(animated: true, completion: nil)
                
            })
            
        }
    }

}
