//
//  AddTaskTableViewController.swift
//  ProjetoFinal-iOS3
//
//  Created by Wesley Brito on 29/06/2018.
//  Copyright © 2018 Wesley Brito. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import UserNotifications
import Vision
import CoreML


class PredictionLocation: NSObject, MKAnnotation{
    var identifier = "Prediction"
    var title: String?
    var coordinate: CLLocationCoordinate2D
    init(name:String,lat:CLLocationDegrees,long:CLLocationDegrees){
        title = name
        coordinate = CLLocationCoordinate2DMake(lat, long)
    }
}

class PredictionLocationList: NSObject {
    var place = [PredictionLocation]()
    override init(){
        place += [PredictionLocation(name:"Local 1",lat: 0, long: 0)]
        place += [PredictionLocation(name:"Local 2",lat: 1, long: 1)]
        place += [PredictionLocation(name:"Local 3",lat: 2, long: 2)]
    }
}



class AddTaskTableViewController: UITableViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var taskImageButton: UIButton!
    @IBOutlet weak var addText: UITextView!
    @IBOutlet weak var addDate: UIDatePicker!
    @IBOutlet weak var taskUIImage: UIImageView!
    @IBOutlet weak var addTaskMapPoint: MKMapView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var segmentNotification: UISegmentedControl!
    
    //chamada model Core Data
    var task: Task!
    var location = CLLocationManager()
    let model = RN1015k500()
    
    
    var pictureString: String = "nil"
    
    var idNotify = ""
    //botao acao da imageview
    @IBAction func addTaskImageButton(_ sender: Any) {
        addImage()
    }
    
    //botao acao bar button item
    @IBAction func saveButton(_ sender: Any) {
        
        saveTask()
        
    }
    
    //MARK: - Map setup
    func resetRegion(){
        let region = MKCoordinateRegionMakeWithDistance(annotation.coordinate, 5000, 5000)
        addTaskMapPoint.setRegion(region, animated: true)
    }
    
    var myLatitude = ""
    var myLongitude = ""
    
    // Array of annotations
    let annotation = MKPointAnnotation()
    var places = PredictionLocationList().place
    
    //Configuracao da localizacao
    func configLocation() {
        location.delegate = self
        location.desiredAccuracy = kCLLocationAccuracyBest
        location.requestWhenInUseAuthorization()
        location.startUpdatingLocation()
    }
    //permissao para localizacao
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status != .authorizedWhenInUse && status != .notDetermined {
            let alertController = UIAlertController(title: "Permissão de localização",
                                                     message: "Necessário permissão para acesso à sua localização!! por favor habilite.",
                                                     preferredStyle: .alert )
            let actionConfig = UIAlertAction(title: "Abrir configurações", style: .default , handler: { (alertConfig) in
                if let config = NSURL(string: UIApplicationOpenSettingsURLString ) {
                    UIApplication.shared.open( config as URL )
                }
            })
            let actionCancel = UIAlertAction(title: "Cancelar", style: .default , handler: nil )
            alertController.addAction( actionConfig )
            alertController.addAction( actionCancel )
            present( alertController , animated: true, completion: nil )
        }
    }
    
    //Função para chamada da camera
    func addImage() {
        let alert = UIAlertController(title: "Seleção de Fotos", message: "Selecione a foto que deseja pesquisar a localização", preferredStyle: .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraAction = UIAlertAction(title: "Câmera", style: .default) { (action: UIAlertAction) in
                self.selectPicture(sourceType: .camera)
            }
            alert.addAction(cameraAction)
        }
        let libraryAction = UIAlertAction(title: "Biblioteca de fotos", style: .default) { (action: UIAlertAction) in
            self.selectPicture(sourceType: .photoLibrary)
        }
            alert.addAction(libraryAction)
        
//        let photosAction = UIAlertAction(title: "Álbum de fotos", style: .default) { (action: UIAlertAction) in
//            self.selectPicture(sourceType: .savedPhotosAlbum)
//        }
//            alert.addAction(photosAction)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        present(alert, animated: true, completion: nil)
        
    }
    
    func selectPicture(sourceType: UIImagePickerControllerSourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func configNotification() {
        idNotify = String(Date().timeIntervalSince1970)
        
        let content = UNMutableNotificationContent()
        content.title = "Lembrete"
        content.body = addText.text
        content.categoryIdentifier = "Lembrete"
        
        //date formatter
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let newDate = dateFormatter.string(from: addDate.date)
        
        content.subtitle = "Tarefa: \(newDate)"
        
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: addDate.date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let request = UNNotificationRequest(identifier: idNotify, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
    
    //funcao de salvar
    func saveTask() {
        
        if task == nil {
            task = Task(context: context)
        }
        
        task.title = addText.text
        task.date = addDate.date
        task.picture = taskUIImage.image
        if segmentNotification.selectedSegmentIndex == 0 {
            configNotification()
        }
        task.notification = Int16(segmentNotification.selectedSegmentIndex)
        task.idNotification = idNotify
        do {
            try context.save()
            alertMessage(title: "Tarefa", message: "Sua tarefa foi salva com sucesso !!!")
        } catch {
            print(error.localizedDescription)
            alertMessage(title: "Tarefa", message: "Erro ao salvar sua tarefa !!!")
        }
    }
    
    func checkEditInfors() {
        
        if task != nil {
            addText.text = task.title
            taskUIImage.image = task.picture as? UIImage
            
            if let editDate = task.date {
                addDate.date = editDate
            }
            if let editIdNotification = task.idNotification {
                 UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [editIdNotification])
            }
            
            if task.picture != nil {
                taskImageButton.setTitle(nil, for: .normal)
            }
            editButton.setTitle("Salvar Edição", for: .normal)
            editButton.addTarget(self, action: #selector(editAction), for: .touchUpInside)
            
        }
        
    }
    
    @objc func editAction() {
        saveTask()
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addDate.minimumDate = Date()
        configLocation()
        checkEditInfors()
        testPrediction()
        addText.becomeFirstResponder()
        
    }
    
    func testPrediction() {
        if taskUIImage.image != nil {
            predictUsingVision(image: taskUIImage.image!)
        } else {
            predictUsingVision(image: #imageLiteral(resourceName: "no-img"))
        }
    }
    
    func predictUsingVision(image: UIImage) {
        guard let visionModel = try? VNCoreMLModel(for: model.model) else {
            fatalError("Something went wrong")
        }
        
        let request = VNCoreMLRequest(model: visionModel) { request, error in
            if let observations = request.results as? [VNClassificationObservation] {
                let top3 = observations.prefix(through: 2)
                    .map { ($0.identifier, Double($0.confidence)) }
                self.show(results: top3)
            }
        }
        
        request.imageCropAndScaleOption = .centerCrop
        
        let handler = VNImageRequestHandler(cgImage: image.cgImage!)
        try? handler.perform([request])
    }
    
    typealias Prediction = (String, Double)
    
    func show(results: [Prediction]) {
        var s: [String] = []
        for (i, pred) in results.enumerated() {
            let latLongArr = pred.0.components(separatedBy: "\t")
            myLatitude = latLongArr[1]
            myLongitude = latLongArr[2]
            s.append(String(format: "%d: %@ %@ (%3.2f%%)", i + 1, myLatitude, myLongitude, pred.1 * 100))
            places[i].title = String(i+1)
            places[i].coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(myLatitude)!, longitude: CLLocationDegrees(myLongitude)!)
        }
        
        resetRegion()
        addTaskMapPoint.centerCoordinate = places[0].coordinate
        addTaskMapPoint.addAnnotations(places)
        zoomMapFitAnnotations()
    }
    
    func zoomMapFitAnnotations() {
        var zoomRect = MKMapRectNull
        for annotation in addTaskMapPoint.annotations {
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
            if (MKMapRectIsNull(zoomRect)) {
                zoomRect = pointRect
            } else {
                zoomRect = MKMapRectUnion(zoomRect, pointRect)
            }
        }
        self.addTaskMapPoint.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(50, 50, 50, 50), animated: true)
    }
    
    func alertMessage(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}

extension AddTaskTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        taskUIImage.image = image
        taskImageButton.setTitle(nil, for: .normal)
        dismiss(animated: true, completion: nil)
        self.predictUsingVision(image: image!)
    }
}
