//
//  TaskListTableViewController.swift
//  ProjetoFinal-iOS3
//
//  Created by Wesley Brito on 02/07/2018.
//  Copyright © 2018 Wesley Brito. All rights reserved.
//

import UIKit
import FirebaseAuth
import FBSDKLoginKit
import GoogleSignIn
import CoreData
import UserNotifications

class TaskListTableViewController: UITableViewController {
    
    var fetchedResultController: NSFetchedResultsController<Task>!
    
    override func viewWillAppear(_ animated: Bool) {
        //checkAuth()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTasks()
        
    }
    
    //Listagem das tarefas
    func loadTasks() {
        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
        let sortDescritor = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sortDescritor]
        
        fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultController.delegate = self
        
        do {
            try fetchedResultController.performFetch()
        } catch {
            print(error.localizedDescription)
            alertMessage(title: "Listar Tarefas", message: "Erro ao listar tarefas")
        }
    }
    

    
    func checkAuth() {
        Auth.auth().addStateDidChangeListener { (Auth, usuario) in
            if usuario != nil {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logoff", style: .plain, target: self, action: #selector (self.handleCustom))
            } else {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginStory") as? LoginViewController {
                    self.present(viewController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    @objc func handleCustom() {
        GIDSignIn.sharedInstance().signOut()
        FBSDKLoginManager().logOut()
        try! Auth.auth().signOut()
        checkAuth()
    }
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = fetchedResultController.fetchedObjects?.count ?? 0
        
        return count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == "editTask" {
            let vc = segue.destination as! AddTaskTableViewController
            
            if let tasks = fetchedResultController.fetchedObjects {
                vc.task = tasks[tableView.indexPathForSelectedRow!.row]
            }
            
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TaskListTableViewCell
        
        guard let task = fetchedResultController.fetchedObjects?[indexPath.row] else { return cell }
        
        cell.prepare(with: task)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let task = fetchedResultController.fetchedObjects?[indexPath.row] else { return }
            context.delete(task)
            guard let notification = task.idNotification else { return }
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [notification])
        }
    }
    
    func alertMessage(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

}

extension TaskListTableViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
            case .delete:
                if let indexPath = indexPath {
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
                break
            default:
                tableView.reloadData()
        }
        
    }
    
}
