//
//  ViewContext.swift
//  ProjetoFinal-iOS3
//
//  Created by Wesley Brito on 02/07/2018.
//  Copyright © 2018 Wesley Brito. All rights reserved.
//

import UIKit
import CoreData

extension UITableViewController {
    
    var context: NSManagedObjectContext {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        return appDelegate.persistentContainer.viewContext
    }
    
    
}
