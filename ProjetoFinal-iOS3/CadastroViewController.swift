//
//  CadastroViewController.swift
//  ProjetoFinal-iOS3
//
//  Created by Wesley Brito on 29/06/2018.
//  Copyright © 2018 Wesley Brito. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class CadastroViewController: UIViewController {
    
    @IBOutlet weak var emailText: UITextField!
    
    @IBOutlet weak var senhaText: UITextField!
    
    @IBOutlet weak var confirmaSenhaText: UITextField!
    
    @IBAction func cadastrarButton(_ sender: Any) {
        
        if let email = self.emailText.text {
            if let senha = self.senhaText.text{
                if let confirmaSenha = self.confirmaSenhaText.text{
                    
                    //valida senha
                    if senha == confirmaSenha{
                        
                        //Autenticar usuario firebase
                        let autenticacao = Auth.auth()
                        autenticacao.createUser(withEmail: email, password: senha, completion: { (usuario, erro) in
                            if erro == nil {
                                if usuario == nil {
                                    self.alertMessage(title: "Erro ao autenticar", message: "Erro ao autenticar, tente novamente.")
                                    print("Erro durante a autenticação")
                                } else {
                                    self.dismiss(animated: true, completion: nil)
                                }
                                
                            } else {
                                self.alertMessage(title: "ERRO", message: "Não foi possível efetuar autenticação.")
                            }
                        })
                    }
                }
            }
        }
    }
    
    func alertMessage(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
